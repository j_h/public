<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlf="urn:oasis:names:tc:xliff:document:1.2" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="html" />
    <xsl:template match="xlf:xliff">
        <html>
            <head>
                <style>
                    table, th, td {
                    border: 1px solid black;
                    border-collapse: collapse;
                    }
                    th, td {
                    padding: 15px;
                    }
                    th {
                    text-align: left;
                    } 
                    tr:nth-child(even) {
                    background-color: #eee;
                    }
                    tr:nth-child(odd) {
                    background-color: #fff;
                    }
                    th {
                    color: white;
                    background-color: black;
                    } 
                </style>
            </head>
            <body>
                <table style="width:100%">
                    <tr>
                        <th>Source</th>
                        <th>Target</th>
                        <th>URLs</th>
                    </tr>
                    <xsl:apply-templates /></table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="xlf:trans-unit">
        <tr>
            <td><xsl:value-of select="xlf:source/text()"/></td>
            <td><xsl:value-of select="xlf:target/text()"/></td>
            <td><ul><xsl:for-each select="xlf:note[@annotates='source'][contains(text(),'url: ')]">
                <li><xsl:value-of select="substring(text(),6)"/></li>
            </xsl:for-each></ul></td>
        </tr>
    </xsl:template>
</xsl:stylesheet>